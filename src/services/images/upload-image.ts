import imageModel from '@modules/image/schema';
import { IImage, Imulterdetails } from '@modules/image/model';
/**
 * Save new image into db
 * @param imageDetails
 */
export const saveImage = async (imageDetails: Imulterdetails): Promise<IImage> => {
  const newImage: IImage = new imageModel({
    name: imageDetails.name,
    type: imageDetails.type,
    description: imageDetails.description,
    url: imageDetails.url,
  });
  await newImage.save();
  return newImage;
};

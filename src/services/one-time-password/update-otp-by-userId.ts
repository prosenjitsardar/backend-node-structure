import oneTimePasswordModel from '@modules/one-time-password/schema';
import { Types } from 'mongoose';
/**
 * update new otp into db
 * @param updateOtpByUserId
 */
export const updateOtpByUserId = async (
  otp: number | null,
  userId: Types.ObjectId,
): Promise<void> => {
  const condition = {
    user: userId,
  };
  const update = { $set: { otp: otp } };

  await oneTimePasswordModel.updateOne(condition, update);
};

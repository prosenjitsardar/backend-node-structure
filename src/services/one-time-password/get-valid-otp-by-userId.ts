import oneTimePasswordModel from '@modules/one-time-password/schema';
import { Types } from 'mongoose';
import { IOtp } from '@modules/one-time-password/model';
/**
 * get OTP details by user Id
 * @param getValidOtpByUserId
 */
export const getValidOtpByUserId = async (userId: Types.ObjectId): Promise<IOtp | null> => {
  const condition = {
    user: userId,
    updatedAt: { $gt: new Date(new Date().getTime() - 1000 * 60 * 10) },
  };
  const selection = {
    __v: 0,
  };

  const validOtpDetails = await oneTimePasswordModel.findOne(condition, selection);
  return validOtpDetails;
};

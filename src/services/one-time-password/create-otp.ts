import oneTimePasswordModel from '@modules/one-time-password/schema';
import { IOtp } from '@modules/one-time-password/model';
import { Types } from 'mongoose';
/**
 * Save new otp into db
 * @param otpDetails
 */
export const createOtp = async (otp: number, userId: Types.ObjectId): Promise<IOtp> => {
  const newOtp: IOtp = new oneTimePasswordModel({
    otp: otp,
    user: userId,
  });

  await newOtp.save();
  return newOtp;
};

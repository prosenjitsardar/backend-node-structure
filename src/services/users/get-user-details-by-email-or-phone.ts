import userModel from '@modules/users/schema';
import { IUser } from '@modules/users/model';

export const getUserDetailsByEmailOrPhone = async (emailOrPhone: string): Promise<IUser | null> => {
  const condition = {
    $or: [
      {
        email: {
          $regex: new RegExp(
            // eslint-disable-next-line no-useless-escape
            '^' + emailOrPhone.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$',
            'i',
          ),
        },
      },
      { phone: emailOrPhone },
    ],
  };
  const selection = {
    __v: 0,
  };
  const userDetails = await userModel.findOne(condition, selection);
  return userDetails;
};

import userModel from '@modules/users/schema';

export const changeisActiveByEmail = async (email: string): Promise<void> => {
  const filter = {
    email: {
      // eslint-disable-next-line no-useless-escape
      $regex: new RegExp('^' + email.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i'),
    },
  };
  const update = { $set: { isActive: true } };

  await userModel.updateOne(filter, update);
};

import userModel from '@modules/users/schema';
import { IUser } from '@modules/users/model';

export const changePassword = async (email: string, newPassword: string): Promise<IUser | null> => {
  const filter = {
    email: {
      // eslint-disable-next-line no-useless-escape
      $regex: new RegExp('^' + email.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i'),
    },
  };
  const update = { $set: { password: newPassword } };

  const userDetails = await userModel.updateOne(filter, update);

  return userDetails;
};

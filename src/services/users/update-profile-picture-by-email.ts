import userModel from '@modules/users/schema';

export const updateProfilePictureByEmail = async (
  email: string,
  profileImage: string,
): Promise<void> => {
  const filter = {
    email: {
      // eslint-disable-next-line no-useless-escape
      $regex: new RegExp('^' + email.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i'),
    },
  };
  const update = { $set: { profileImage: profileImage } };

  await userModel.updateOne(filter, update);
};

import templateModel from '@modules/email-templates/schema';
import { ITemplate } from '@modules/email-templates/model';

export const getTemplateByName = async (name: string): Promise<ITemplate | null> => {
  const condition = {
    name: name,
  };
  const selection = {
    __v: 0,
  };
  const templateDetails = await templateModel.findOne(condition, selection);
  return templateDetails;
};

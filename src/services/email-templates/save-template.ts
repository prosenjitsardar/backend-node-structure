import templateModel from '@modules/email-templates/schema';
import { ICreateTemplateRequest, ITemplate } from '@modules/email-templates/model';

/**
 * Save new Template into db
 * @param templateDetails
 */
export const saveTemplate = async (templateDetails: ICreateTemplateRequest): Promise<ITemplate> => {
  const newTemplate: ITemplate = new templateModel({
    name: templateDetails.name,
    subject: templateDetails.subject,
    body: templateDetails.body,
  });

  await newTemplate.save();
  return newTemplate;
};

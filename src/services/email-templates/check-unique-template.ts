import templateModel from '@modules/email-templates/schema';

export const checkUniqueTemplate = async (name: string): Promise<boolean> => {
  const condition = {
    name: name,
  };
  const templateCount = await templateModel.countDocuments(condition);

  return templateCount > 0 ? false : true;
};

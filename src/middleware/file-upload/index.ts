import multer from 'multer';
import { resolve } from 'path';

/*
|-----------------------------------
|   Multer Configuration
|-----------------------------------
*/
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, resolve(process.cwd(), 'src/public/images'));
  },

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  filename: function (req: any, file: any, cb: any) {
    const filename =
      'Img-' +
      req.body.type +
      '-' +
      Math.floor(Math.random() * 1000000000) +
      '-' +
      Math.floor(Date.now() / 1000) +
      '.' +
      file.mimetype.split('/')[1];
    cb(null, filename);
  },
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const fileFilter = (req: any, file: any, cb: any) => {
  if (
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg' ||
    file.mimetype === 'image/png'
  ) {
    cb(null, true);
  } else {
    cb(new Error('Image uploaded is not of type jpg/jpeg or png'), false);
  }
};

export const upload = multer({ storage: storage, fileFilter: fileFilter }).single('images');

/*
  |-----------------------------------
  |   Multer Configuration Ends Here
  |-----------------------------------
  */

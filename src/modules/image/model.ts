import { Document } from 'mongoose';

export interface IImage extends Document {
  name: string;
  type: string;
  description: string;
  url: string;
}

export interface Imulterdetails {
  name: string;
  type: string;
  description: string;
  url: string;
}

export interface IUploadImageRequest {
  type: string;
  description: string;
}

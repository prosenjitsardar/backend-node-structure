import mongoose, { Schema } from 'mongoose';
import { IImage } from './model';

const ImageSchema: Schema = new Schema(
  {
    name: { type: String, required: true, trim: true },
    type: { type: String, required: true, trim: true, enum: ['action', 'comedy'] },
    description: { type: String, required: true },
    url: { type: String, required: true },
  },
  {
    timestamps: true,
  },
);

// Export the model and return your IUser interface
export default mongoose.model<IImage>('imageModel', ImageSchema, 'images');

import { Document } from 'mongoose';
interface IUserName extends Document {
  first: string;
  last: string;
}

export interface IUser extends Document {
  name: IUserName;
  email: string;
  phone: string;
  isActive: boolean;
  password: string;
  profileImage: string;
}

export interface ICreateUserRequest {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  password: string;
}

export interface ILoginUserRequest {
  emailOrPhone: string;
  password: string;
}

export interface ILoginWithOtpRequest {
  email: string;
  otp: number;
}

export interface IResendOtpRequest {
  emailOrPhone: string;
}

export interface IUserTokens {
  name: IUserName;
  email: string;
  accessToken: string;
  accessTokenExpiry: number;
  refreshToken: string;
  refreshTokenExpiry: number;
  phone: string;
}

export interface IChangePasswordRequest {
  password: string;
  newPassword: string;
}

export interface IForgetPasswordRequest {
  emailOrPhone: string;
}

export interface IResetPasswordRequest {
  emailOrPhone: string;
  otp: number;
  newPassword: string;
}

export interface IUserRequestObject {
  _id: string;
  name: IUserName;
  email: string;
  phone: string;
  profileImage: string;
}

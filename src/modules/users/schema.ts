import mongoose, { Schema } from 'mongoose';
import { IUser } from './model';

const UserSchema: Schema = new Schema(
  {
    name: {
      first: { type: String, required: true, trim: true },
      last: { type: String, require: true, trim: true },
    },
    email: { type: String, required: [true, 'User email required'], unique: true, trim: true },
    phone: {
      type: String,
      required: [true, 'User phone number required'],
      unique: true,
      trim: true,
    },
    password: { type: String, required: [true, 'Password required'] },
    profileImage: { type: String, default: null },
    isActive: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  },
);

// Export the model and return your IUser interface
export default mongoose.model<IUser>('userModel', UserSchema, 'users');

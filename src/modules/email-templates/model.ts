import { Document } from 'mongoose';

export interface ITemplate extends Document {
  name: string;
  subject: string;
  body: string;
}

export interface ICreateTemplateRequest {
  name: string;
  subject: string;
  body: string;
}

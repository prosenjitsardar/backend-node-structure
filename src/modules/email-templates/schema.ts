import mongoose, { Schema } from 'mongoose';
import { ITemplate } from './model';

const TemplateSchema: Schema = new Schema(
  {
    name: { type: String, required: true, unique: true, trim: true },
    subject: { type: String, required: true, trim: true },
    body: { type: String, required: true, trim: true },
  },
  {
    timestamps: true,
  },
);

// Export the model and return your ITemplate interface
export default mongoose.model<ITemplate>('templateModel', TemplateSchema, 'templates');

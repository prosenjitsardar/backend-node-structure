export enum ITemplateName {
  'signup' = 'SIGNUP',
  'resendOtp' = 'RESEND-OTP',
  'forgotPassword' = 'FORGOT-PASSWORD',
  'resetPassword' = 'RESET-PASSWORD',
}

export interface IMailOptions {
  receiverEmail: string | string[];
  data?: string | number;
  template: ITemplateName;
}

import mongoose, { Schema } from 'mongoose';
import { IOtp } from './model';

const OneTimePasswordSchema: Schema = new Schema(
  {
    otp: { type: Number, required: true },
    user: { type: Schema.Types.ObjectId, ref: 'users', required: true },
  },
  {
    timestamps: true,
  },
);

// Export the model and return your IUser interface
export default mongoose.model<IOtp>(
  'oneTimePasswordModel',
  OneTimePasswordSchema,
  'oneTimePasswords',
);

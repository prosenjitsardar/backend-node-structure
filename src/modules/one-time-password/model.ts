import { Document, Types } from 'mongoose';

export interface IOtp extends Document {
  otp: number | null;
  user: Types.ObjectId;
}

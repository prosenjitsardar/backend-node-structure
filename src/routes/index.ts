import { Router } from 'express';
import { userRouter } from './user';
import { imageRouter } from './image';
import { templateRouter } from './email-template';

const v1Router = Router();

v1Router.use('/user', userRouter);
v1Router.use('/image', imageRouter);
v1Router.use('/template', templateRouter);
// All routes go here

export { v1Router };

import { Router } from 'express';
import { imageController } from '@controllers/index';
import { validateApiKey } from '@middleware/index';
import { uploadImageValidator } from '@validations/image';
import { upload } from '@middleware/index';

const imageRouter = Router();

imageRouter.use(validateApiKey);

imageRouter.post('/upload-image', upload, uploadImageValidator, imageController.uploadImage);

export { imageRouter };

import { Router } from 'express';
import { userController } from '@controllers/index';
import {
  validateApiKey,
  validateUserAccessToken,
  validateUserRefreshToken,
  upload,
} from '@middleware/index';

import {
  userSignup,
  userLogin,
  changePassword,
  userLoginWithOtp,
  resendOtp,
  forgetPassword,
  resetPassword,
  uploadImageValidator,
} from '@validations/user';

const userRouter = Router();

userRouter.use(validateApiKey);

userRouter.post('/signup', userSignup, userController.createUser);

userRouter.post('/resend-otp', resendOtp, userController.resendOtp);

userRouter.post('/login-with-otp', userLoginWithOtp, userController.loginWithOtp);

userRouter.post('/login', userLogin, userController.useLogin);

userRouter.get('/me', validateUserAccessToken, userController.getCurrentUserDetails);

userRouter.get('/token', validateUserRefreshToken, userController.genrateNewToken);

userRouter.post('/forgot-password', forgetPassword, userController.forgotPassword);

userRouter.post('/reset-password', resetPassword, userController.resetPassword);

userRouter.post(
  '/change-password',
  validateUserAccessToken,
  changePassword,
  userController.changePassword,
);

userRouter.post(
  '/profile-image',
  validateUserAccessToken,
  uploadImageValidator,
  upload,
  userController.uploadProfileImage,
);

export { userRouter };

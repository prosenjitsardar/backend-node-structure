import { Router } from 'express';
import { validateApiKey } from '@middleware/index';
import { createTemplateValidation } from '@validations/email-template';
import { templateController } from '@controllers/index';

const templateRouter = Router();

templateRouter.use(validateApiKey);

templateRouter.post(
  '/create-template',
  createTemplateValidation,
  templateController.createTemplate,
);

export { templateRouter };

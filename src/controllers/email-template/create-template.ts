import { Request, Response } from 'express';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { ICreateTemplateRequest } from '@modules/email-templates/model';
import { templateService } from '@services/index';

export const createTemplate = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: ICreateTemplateRequest = req.body;

    const isTemplateUnique = await templateService.checkUniqueTemplate(reqBody.name);
    if (!isTemplateUnique) {
      throw StatusError.badRequest('duplicateTemplate');
    }

    await templateService.saveTemplate(reqBody);
    res.sendStatus(201);
  },
);

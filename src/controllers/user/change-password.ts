import { Request, Response } from 'express';
import { userService } from '@services/index';
import { IChangePasswordRequest } from '@modules/users/model';
import bcrypt from 'bcrypt';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { envs } from '@config/env';

export const changePassword = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: IChangePasswordRequest = req.body;

    // Getting users details
    const userDetails = await userService.getUserDetailsByEmail(req.userDetails.email);
    if (!userDetails) {
      throw StatusError.badRequest('invaildEmailOrPassword');
    }
    if (userDetails.isActive === false) {
      throw StatusError.badRequest('userIsNotActive');
    }

    // password compare process
    const isSame = await bcrypt.compare(reqBody.password, userDetails.password);
    if (!isSame) {
      throw StatusError.badRequest('invalidPassword');
    }

    // Creating new password
    const newPassword = await bcrypt.hash(reqBody.newPassword, envs.passwordSalt);

    // Saving new password
    await userService.changePassword(req.userDetails.email, newPassword);

    res.sendStatus(204);
  },
);

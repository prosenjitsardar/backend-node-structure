import { Request, Response } from 'express';
import { userService, oneTimePasswordService } from '@services/index';
import { IForgetPasswordRequest } from '@modules/users/model';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { IMailOptions, ITemplateName } from '@modules/emails/model';
import { Mailer, generateOtp } from '@helper/index';

export const forgotPassword = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: IForgetPasswordRequest = req.body;

    // Getting users details
    const userDetails = await userService.getUserDetailsByEmailOrPhone(reqBody.emailOrPhone);
    if (!userDetails) {
      throw StatusError.badRequest('invalidEmailOrPhone');
    }
    if (userDetails.isActive === false) {
      throw StatusError.badRequest('userIsNotActive');
    }

    // Generating a new 6 digit OTP
    const otp = generateOtp();

    // Updating new OTP into OTP document
    await oneTimePasswordService.updateOtpByUserId(otp, userDetails._id);

    // Setting up OTP Email for the user
    const mailOptions: IMailOptions = {
      receiverEmail: userDetails.email,
      data: otp,
      template: ITemplateName.forgotPassword,
    };
    const mailer = new Mailer(mailOptions);

    res.sendStatus(204);

    // Sending Email to the user
    mailer.sentMail();
  },
);

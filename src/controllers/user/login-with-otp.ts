import { Request, Response } from 'express';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { ILoginWithOtpRequest } from '@modules/users/model';
import { userService, oneTimePasswordService } from '@services/index';

export const loginWithOtp = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: ILoginWithOtpRequest = req.body;

    // get user details by email
    const userDetails = await userService.getUserDetailsByEmail(reqBody.email);
    if (!userDetails) {
      throw StatusError.badRequest('invaildEmailOrOtp');
    }

    // Get users OTP details by users Id
    const otpDetails = await oneTimePasswordService.getValidOtpByUserId(userDetails._id);
    if (!otpDetails) {
      throw StatusError.badRequest('invaildEmailOrOtp');
    }

    // Comparing OTP
    if (otpDetails.otp !== reqBody.otp) {
      throw StatusError.badRequest('invaildEmailOrOtp');
    }

    // Erasing OTP of OTP document
    await oneTimePasswordService.updateOtpByUserId(null, userDetails._id);

    // Changing isActive status of user
    await userService.changeisActiveByEmail(userDetails.email);

    res.send(userService.genrateUserTokens(userDetails));
  },
);

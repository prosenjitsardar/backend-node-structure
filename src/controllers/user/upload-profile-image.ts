import { Request, Response } from 'express';
import { userService } from '@services/index';
import { controller } from '@config/controller/controller';

export const uploadProfileImage = controller(
  async (req: Request, res: Response): Promise<void> => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const reqFile: any = req.file;
    const profileImage = `images/${reqFile.filename}`;

    await userService.updateProfilePictureByEmail(req.userDetails.email, profileImage);
    res.sendStatus(204);
  },
);

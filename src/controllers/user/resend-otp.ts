import { Request, Response } from 'express';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { IResendOtpRequest } from '@modules/users/model';
import { userService, oneTimePasswordService } from '@services/index';
import { IMailOptions, ITemplateName } from '@modules/emails/model';
import { Mailer, generateOtp } from '@helper/index';

export const resendOtp = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: IResendOtpRequest = req.body;

    // get user details by email
    const userDetails = await userService.getUserDetailsByEmailOrPhone(reqBody.emailOrPhone);
    if (!userDetails) {
      throw StatusError.badRequest('invalidEmail');
    }

    // Generating a new 6 digit OTP
    const otp = generateOtp();

    // Updating new OTP into OTP document
    await oneTimePasswordService.updateOtpByUserId(otp, userDetails._id);

    // Setting up Email for the user
    const mailOptions: IMailOptions = {
      receiverEmail: userDetails.email,
      data: otp,
      template: ITemplateName.resendOtp,
    };
    const mailer = new Mailer(mailOptions);

    res.sendStatus(204);

    // Sending Email to the user
    mailer.sentMail();
  },
);

import { Request, Response } from 'express';
import { userService, oneTimePasswordService } from '@services/index';
import { IResetPasswordRequest } from '@modules/users/model';
import bcrypt from 'bcrypt';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { envs } from '@config/env';
import { IMailOptions, ITemplateName } from '@modules/emails/model';
import { Mailer } from '@helper/index';

export const resetPassword = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: IResetPasswordRequest = req.body;

    // Getting users details
    const userDetails = await userService.getUserDetailsByEmailOrPhone(reqBody.emailOrPhone);
    if (!userDetails) {
      throw StatusError.badRequest('invaildEmailOrOtp');
    }
    if (userDetails.isActive === false) {
      throw StatusError.badRequest('userIsNotActive');
    }

    // Get users OTP details by users Id
    const otpDetails = await oneTimePasswordService.getValidOtpByUserId(userDetails._id);
    if (!otpDetails) {
      throw StatusError.badRequest('invaildEmailOrOtp');
    }

    // Comparing OTP
    if (otpDetails.otp !== reqBody.otp) {
      throw StatusError.badRequest('invaildEmailOrOtp');
    }

    // Erasing OTP of OTP document
    await oneTimePasswordService.updateOtpByUserId(null, userDetails._id);

    // Creating new password
    const newPassword = await bcrypt.hash(reqBody.newPassword, envs.passwordSalt);

    // Saving new password
    await userService.changePassword(userDetails.email, newPassword);

    // Setting up Notification Email for the user
    const mailOptions: IMailOptions = {
      receiverEmail: userDetails.email,
      template: ITemplateName.resetPassword,
    };
    const mailer = new Mailer(mailOptions);

    res.sendStatus(204);

    // Sending Email to user
    mailer.sentMail();
  },
);

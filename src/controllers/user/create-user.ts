import { Request, Response } from 'express';
import { userService, oneTimePasswordService } from '@services/index';
import { ICreateUserRequest } from '@modules/users/model';
import bcrypt from 'bcrypt';
import { controller } from '@config/controller/controller';
import { StatusError } from '@config/statusError/statusError';
import { envs } from '@config/env';
import { IMailOptions, ITemplateName } from '@modules/emails/model';
import { Mailer } from '@helper/index';
import { generateOtp } from '@helper/index';

export const createUser = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: ICreateUserRequest = req.body;

    // existing user verification
    const isEmailUnique = await userService.checkUniqueEmail(reqBody.email);
    if (!isEmailUnique) {
      throw StatusError.badRequest('duplicateEmail');
    }

    // Hashing password
    reqBody.password = await bcrypt.hash(reqBody.password, envs.passwordSalt);

    // Saving user
    const userDetails = await userService.saveUser(reqBody);

    // Generating 6 digit OTP & Creating OTP Document for new user
    const otp = generateOtp();
    await oneTimePasswordService.createOtp(otp, userDetails._id);

    // Setting up Email for the user
    const mailOptions: IMailOptions = {
      receiverEmail: reqBody.email,
      data: otp,
      template: ITemplateName.signup,
    };
    const mailer = new Mailer(mailOptions);

    res.sendStatus(204);

    // Sending Email to the user
    mailer.sentMail();
  },
);

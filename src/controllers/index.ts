import * as userController from './user';
import * as imageController from './image';
import * as templateController from './email-template';

export { userController, imageController, templateController };

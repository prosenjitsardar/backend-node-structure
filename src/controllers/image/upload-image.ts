import { Request, Response } from 'express';
import { imageService } from '@services/index';
import { IUploadImageRequest, Imulterdetails } from '@modules/image/model';
import { controller } from '@config/controller/controller';

export const uploadImage = controller(
  async (req: Request, res: Response): Promise<void> => {
    const reqBody: IUploadImageRequest = req.body;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const reqFile: any = req.file;

    const imageDetails: Imulterdetails = {
      name: reqFile.filename,
      type: reqBody.type,
      description: reqBody.description,
      url: `/images/${reqFile.filename}`,
    };

    await imageService.saveImage(imageDetails);
    res.sendStatus(201);
  },
);

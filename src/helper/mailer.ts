import { IMailOptions } from '@modules/emails/model';
import { ITemplate } from '@modules/email-templates/model';
import { StatusError } from '@config/statusError/statusError';
import { templateService } from '@services/index';
import { envs } from '@config/env';
import * as handlebars from 'handlebars';
import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';

// Creating transporter
const transporter = nodemailer.createTransport(
  smtpTransport({
    service: envs.MAIL_SERVICE,
    host: envs.MAIL_HOST,
    auth: {
      user: envs.MAIL_USERNAME,
      pass: envs.MAIL_USER_PASSWORD,
    },
  }),
);

/*
|-----------------------------------
|   Mail Configuration
|-----------------------------------
*/
class Mailer {
  receiverEmail: string | string[];
  data: string | number | undefined | null;
  template: string;

  constructor(mailOptions: IMailOptions) {
    if (mailOptions.receiverEmail && mailOptions.template) {
      this.receiverEmail = mailOptions.receiverEmail;
      this.data = mailOptions.data;
      this.template = mailOptions.template;
    } else {
      throw StatusError.serverError('pleaseCheckMailOptions');
    }
  }

  async sentMail(): Promise<void> {
    // Getting template details
    const templateDetails: ITemplate | null = await templateService.getTemplateByName(
      this.template,
    );
    if (templateDetails == null) {
      throw StatusError.badRequest('NoTemplateFoundWithThisName');
    }

    // Compiling mail template and data with handlebars
    const handlebarTemplate = handlebars.compile(templateDetails.body);
    const htmlToSend = handlebarTemplate({
      data: this.data,
    });

    // Setting up email
    const email = {
      from: envs.MAIL_FROM_NAME + ' <' + envs.MAIL_FROM_ADDRESS + '>',
      to: this.receiverEmail,
      subject: templateDetails.subject,
      html: htmlToSend,
    };

    //Sending email
    transporter.sendMail(email, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent:' + info.response);
      }
    });
  }
}

export { Mailer };
/*
  |-----------------------------------
  |   Mail Configuration Ends Here
  |-----------------------------------
  */

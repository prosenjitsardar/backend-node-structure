import { Mailer } from './mailer';
import { generateOtp } from './generate-otp';

export { Mailer, generateOtp };

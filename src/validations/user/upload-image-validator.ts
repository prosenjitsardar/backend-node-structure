import { celebrate, Joi } from 'celebrate';

export const uploadImageValidator = celebrate({
  body: Joi.object({
    type: Joi.string().valid('profileImage', 'other').required(),
  }),
});

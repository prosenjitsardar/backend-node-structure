import { celebrate, Joi } from 'celebrate';

export const changePassword = celebrate({
  body: Joi.object({
    password: Joi.string().required().min(6),
    newPassword: Joi.string().required().min(6),
  }),
});

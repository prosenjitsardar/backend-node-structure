import { celebrate, Joi } from 'celebrate';

export const userLoginWithOtp = celebrate({
  body: Joi.object({
    email: Joi.string().email().required(),
    otp: Joi.number().required(),
  }),
});

import { celebrate, Joi } from 'celebrate';

export const userLogin = celebrate({
  body: Joi.object({
    emailOrPhone: [Joi.string().email(), Joi.string().length(10).regex(/^\d+$/)],
    password: Joi.string().required().min(6),
  }),
});

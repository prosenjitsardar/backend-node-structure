import { celebrate, Joi } from 'celebrate';

export const resendOtp = celebrate({
  body: Joi.object({
    emailOrPhone: [Joi.string().email(), Joi.string().length(10).regex(/^\d+$/)],
  }),
});

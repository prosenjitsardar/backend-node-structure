import { celebrate, Joi } from 'celebrate';

export const forgetPassword = celebrate({
  body: Joi.object({
    emailOrPhone: [Joi.string().email(), Joi.string().length(10).regex(/^\d+$/)],
  }),
});

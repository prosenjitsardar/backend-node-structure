import { celebrate, Joi } from 'celebrate';

export const resetPassword = celebrate({
  body: Joi.object({
    emailOrPhone: [Joi.string().email(), Joi.string().length(10).regex(/^\d+$/)],
    otp: Joi.number().required(),
    newPassword: Joi.string().required().min(6),
  }),
});

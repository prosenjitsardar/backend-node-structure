import { userLoginWithOtp } from './login-with-otp';
import { userSignup } from '@validations/user/signup';
import { userLogin } from '@validations/user/login';
import { changePassword } from '@validations/user/change-password';
import { resendOtp } from './resend-otp';
import { forgetPassword } from './forget-password';
import { resetPassword } from './reset-password';
import { uploadImageValidator } from './upload-image-validator';

export {
  userSignup,
  userLogin,
  changePassword,
  userLoginWithOtp,
  resendOtp,
  forgetPassword,
  resetPassword,
  uploadImageValidator,
};

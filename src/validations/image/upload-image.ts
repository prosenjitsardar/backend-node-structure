import { celebrate, Joi } from 'celebrate';

export const uploadImageValidator = celebrate({
  body: Joi.object({
    type: Joi.string().valid('action', 'comedy').required(),
    description: Joi.string().required().min(3),
  }),
});

import { celebrate, Joi } from 'celebrate';

export const createTemplateValidation = celebrate({
  body: Joi.object({
    name: Joi.string().required().min(3),
    subject: Joi.string().required().min(3),
    body: Joi.string().required().min(3),
  }),
});
